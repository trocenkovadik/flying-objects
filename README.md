## Clone the repo

git clone https://gitlab.com/trocenkovadik/flying-objects.git

## Development setup

1. npm i
2. npm run dev

## Production setup

1. npm i
2. npm run build
3. Deploy to the cloud platform

## CSS

Write CSS in `src/style.css` it compiles automatically

## JS

Write JS following folder structure, separate view from logic

## HTML

Write html in `src/index.html`, there is should be static view