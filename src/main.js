import Circle from "./classes/Circle";
import Square from "./classes/Square";
import MovingObject from "./classes/MovingObject";
import Triangle from "./classes/Triangle";

document.addEventListener("DOMContentLoaded", function () {
  const map = L.map("map", {
    center: [58.5953, 25.0136], // Centered over Estonia
    zoom: 7,
    rotate: true,
    worldCopyJump: true,
    maxBounds: L.latLngBounds(L.latLng(-90, -Infinity), L.latLng(90, Infinity)),
    maxBoundsViscosity: 1,
  });

  map.doubleClickZoom.disable();

  L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    bounds: [
      [-90, -180],
      [90, 180],
    ],
    maxZoom: 11,
    minZoom: 3,
  }).addTo(map);

  const objects = {
    squares: [],
    circles: [],
    triangles: [],
  };

  const visibility = {
    squaresVisible: true,
    circlesVisible: true,
    trianglesVisible: true,
  };

  function createObject(type) {
    MovingObject.closeModal();
    const startPosition = L.latLng(59.437, 24.7535);
    let obj;
    switch (type) {
      case "square":
        obj = new Square(map, startPosition);
        objects.squares.push(obj);
        break;
      case "circle":
        obj = new Circle(map, startPosition);
        objects.circles.push(obj);
        break;
      case "triangle":
        obj = new Triangle(map, startPosition);
        objects.triangles.push(obj);
        break;
    }
    return obj;
  }

  function toggleDisplay(type) {
    const isVisible = visibility[type + "Visible"];

    objects[type].forEach((obj) => {
      if (isVisible) {
        map.removeLayer(obj.marker);
        map.removeLayer(obj.startMarker);
        if (obj.polyline && obj.polyline._map) {
          map.removeLayer(obj.polyline);
          obj.polylineVisible = false;
        }
        if (obj.fullTrajectoryPolyline && obj.fullTrajectoryPolyline._map) {
          map.removeLayer(obj.fullTrajectoryPolyline);
        }
        if (obj === MovingObject.currentObject) {
          MovingObject.closeModal();
        }
      } else {
        map.addLayer(obj.marker);
        map.addLayer(obj.startMarker);
        if (obj === MovingObject.currentObject) {
          obj.showDetails();
        }
        if (obj.polyline && !obj.polyline._map && obj.polylineVisible) {
          map.addLayer(obj.polyline);
          obj.polyline.setLatLngs(obj.trajectory);
        }
      }
    });

    visibility[type + "Visible"] = !isVisible;

    const addButtons = document.querySelectorAll(".add-object-button");
    addButtons.forEach((button) => {
      const objectType = button.dataset.type;
      if (!visibility[objectType + "Visible"]) {
        button.disabled = true;
      } else {
        button.disabled = false;
      }
    });
  }

  function rotateMap(deg) {
    map.setBearing(map.getBearing() + deg);
  }

  function moveMap(direction) {
    const panOffset = 0.5;
    const center = map.getCenter();
    switch (direction) {
      case "up":
        map.panTo([center.lat + panOffset, center.lng]);
        break;
      case "down":
        map.panTo([center.lat - panOffset, center.lng]);
        break;
      case "left":
        map.panTo([center.lat, center.lng - panOffset]);
        break;
      case "right":
        map.panTo([center.lat, center.lng + panOffset]);
        break;
    }
  }

  let moveInterval = null;

  function startMoveMap(direction) {
    if (moveInterval) return;
    moveInterval = setInterval(() => moveMap(direction), 20);
  }

  function stopMoveMap() {
    clearInterval(moveInterval);
    moveInterval = null;
  }

  const controlDiv = L.control({ position: "topright" });
  controlDiv.onAdd = function () {
    const div = L.DomUtil.create("div", "control-container");
    div.innerHTML = `
      <div>Flying Object Map</div>
      <button class="add-object-button" data-type="squares" onclick="createObject('square')">Add Square</button>
      <button class="add-object-button" data-type="circles" onclick="createObject('circle')">Add Circle</button>
      <button class="add-object-button" data-type="triangles" onclick="createObject('triangle')">Add Triangle</button>
      <div>Toggle display</div>
      <label><input type="checkbox" checked onchange="toggleDisplay('squares')"> Squares</label>
      <label><input type="checkbox" checked onchange="toggleDisplay('circles')"> Circles</label>
      <label><input type="checkbox" checked onchange="toggleDisplay('triangles')"> Triangles</label>
    `;
    return div;
  };
  controlDiv.addTo(map);

  window.createObject = createObject;
  window.toggleDisplay = toggleDisplay;
  window.moveMap = moveMap;
  window.rotateMap = rotateMap;
});
