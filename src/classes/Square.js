import { getRandomSpeed } from "../utils/getRandomSpeed";
import MovingObject from "./MovingObject";

class Square extends MovingObject {
  constructor(map, startPosition) {
    super("square", map, startPosition);
    this.waypoints = [
      [59.437, 24.7535],
      [-60.409962, -158.203125],
      [-50.409962, -140.203125],
      [59.437, 24.7535],
    ];
    this.currentWaypointIndex = 0;
    this.position = L.latLng(this.waypoints[this.currentWaypointIndex]);
    this.destination = L.latLng(this.waypoints[this.currentWaypointIndex + 1]);
    this.distance = this.calculateDistance();
    this.travelledDistance = 0;
    this.maxTime = 60 * 60 * 1000;
    this.timeout = setTimeout(() => this.remove(), this.maxTime);
    this.speed = this.getSpeed();
  }

  calculateDistance() {
    if (!this.destination) return 0;
    const lat1 = this.position.lat * (Math.PI / 180);
    const lon1 = this.position.lng * (Math.PI / 180);
    const lat2 = this.destination.lat * (Math.PI / 180);
    const lon2 = this.destination.lng * (Math.PI / 180);
    const dlat = lat2 - lat1;
    const dlon = lon2 - lon1;
    const a =
      Math.sin(dlat / 2) ** 2 +
      Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlon / 2) ** 2;
    return 6371 * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  }

  calculateDistanceBetweenPoints(point1, point2) {
    const lat1 = point1.lat * (Math.PI / 180);
    const lon1 = point1.lng * (Math.PI / 180);
    const lat2 = point2.lat * (Math.PI / 180);
    const lon2 = point2.lng * (Math.PI / 180);
    const dlat = lat2 - lat1;
    const dlon = lon2 - lon1;
    const a =
      Math.sin(dlat / 2) ** 2 +
      Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlon / 2) ** 2;
    return 6371 * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  }

  generateNewPosition() {
    if (!this.destination) return this.position;

    const R = 6371;
    const d = this.speed / 3600 / R;
    const bearing = this.bearingToDestination();
    const lat1 = this.position.lat * (Math.PI / 180);
    const lon1 = this.position.lng * (Math.PI / 180);
    const lat2 = Math.asin(
      Math.sin(lat1) * Math.cos(d) +
        Math.cos(lat1) * Math.sin(d) * Math.cos(bearing)
    );
    const lon2 =
      lon1 +
      Math.atan2(
        Math.sin(bearing) * Math.sin(d) * Math.cos(lat1),
        Math.cos(d) - Math.sin(lat1) * Math.sin(lat2)
      );
    const newLat = lat2 * (180 / Math.PI);
    const newLng = lon2 * (180 / Math.PI);
    const newPosition = L.latLng(newLat, newLng);
    this.travelledDistance += this.calculateDistanceBetweenPoints(
      this.position,
      newPosition
    );

    if (this.travelledDistance >= this.distance) {
      this.travelledDistance = 0;
      this.currentWaypointIndex =
        (this.currentWaypointIndex + 1) % this.waypoints.length;
      this.position = L.latLng(this.waypoints[this.currentWaypointIndex]);
      this.destination = L.latLng(
        this.waypoints[(this.currentWaypointIndex + 1) % this.waypoints.length]
      );
      this.distance = this.calculateDistance();
    } else {
      this.position = newPosition;
    }

    return this.position;
  }

  bearingToDestination() {
    const lat1 = this.position.lat * (Math.PI / 180);
    const lon1 = this.position.lng * (Math.PI / 180);
    const lat2 = this.destination.lat * (Math.PI / 180);
    const lon2 = this.destination.lng * (Math.PI / 180);
    const dlon = lon2 - lon1;
    const y = Math.sin(dlon) * Math.cos(lat2);
    const x =
      Math.cos(lat1) * Math.sin(lat2) -
      Math.sin(lat1) * Math.cos(lat2) * Math.cos(dlon);
    return Math.atan2(y, x);
  }

  createGeodesicPath() {
    const fullPath = [];
    for (let i = 0; i < this.waypoints.length; i++) {
      const currentWaypoint = L.latLng(this.waypoints[i]);
      const nextWaypoint = L.latLng(
        this.waypoints[(i + 1) % this.waypoints.length]
      );
      const segment = this.getIntermediatePoints(
        currentWaypoint,
        nextWaypoint,
        100
      );
      fullPath.push(...segment);
    }
    this.geodesicLine = L.polyline(fullPath, {
      color: this.getColor(),
      weight: 3,
      opacity: 0.7,
      dashArray: "5, 10",
    });
    this.geodesicLines.push(this.geodesicLine);
  }

  getIntermediatePoints(start, end, numPoints) {
    const lat1 = start.lat * (Math.PI / 180);
    const lon1 = start.lng * (Math.PI / 180);
    const lat2 = end.lat * (Math.PI / 180);
    const lon2 = end.lng * (Math.PI / 180);
    const d = this.calculateDistanceBetweenPoints(start, end) / 6371;
    const bearing = this.bearingBetweenPoints(start, end);
    const points = [];

    for (let i = 0; i <= numPoints; i++) {
      const f = i / numPoints;
      const A = Math.sin((1 - f) * d) / Math.sin(d);
      const B = Math.sin(f * d) / Math.sin(d);
      const x =
        A * Math.cos(lat1) * Math.cos(lon1) +
        B * Math.cos(lat2) * Math.cos(lon2);
      const y =
        A * Math.cos(lat1) * Math.sin(lon1) +
        B * Math.cos(lat2) * Math.sin(lon2);
      const z = A * Math.sin(lat1) + B * Math.sin(lat2);
      const newLat = Math.atan2(z, Math.sqrt(x * x + y * y)) * (180 / Math.PI);
      const newLng = Math.atan2(y, x) * (180 / Math.PI);

      if (isNaN(newLat) || isNaN(newLng)) {
        console.error(`Invalid intermediate point: (${newLat}, ${newLng})`);
      } else {
        points.push(L.latLng(newLat, newLng));
      }
    }
    return points;
  }

  bearingBetweenPoints(start, end) {
    const lat1 = start.lat * (Math.PI / 180);
    const lon1 = start.lng * (Math.PI / 180);
    const lat2 = end.lat * (Math.PI / 180);
    const lon2 = end.lng * (Math.PI / 180);
    const dlon = lon2 - lon1;
    const y = Math.sin(dlon) * Math.cos(lat2);
    const x =
      Math.cos(lat1) * Math.sin(lat2) -
      Math.sin(lat1) * Math.cos(lat2) * Math.cos(dlon);
    return Math.atan2(y, x);
  }

  getSpeed() {
    return getRandomSpeed(50, 80);
  }

  getFullPath() {
    return this.waypoints;
  }
}

export default Square;
