import MovingObject from "./MovingObject";
import { getRandomSpeed } from "../utils/getRandomSpeed";

class Circle extends MovingObject {
  constructor(map, startPosition) {
    super("circle", map, startPosition);
    this.radius = getRandomSpeed(10000, 30000) / 1000;
    this.angle = 0;
    this.fullCircle = 2 * Math.PI * this.radius;
    this.travelledDistance = 0;
    this.maxTime = 60 * 60 * 1000;

    this.pathCoords = this.getFullPath();
    this.currentCoordIndex = 0;
    this.speed = getRandomSpeed(110, 300);
    this.position = this.pathCoords[0];
    this.marker.setLatLng(this.position);

    this.trajectory = [];
    this.fullPathPolyline = null;

    setTimeout(() => {
      this.startMovement();
    }, 200);
  }

  getFullPath() {
    const path = [];
    const steps = 100;
    for (let i = 0; i <= steps; i++) {
      const angle = (i * 2 * Math.PI) / steps;
      const newLat =
        this.startPosition.lat + (Math.sin(angle) * this.radius) / 111;
      const newLng =
        this.startPosition.lng +
        (Math.cos(angle) * this.radius) /
          (111 * Math.cos(this.degToRad(this.startPosition.lat)));
      path.push(L.latLng(newLat, newLng));
    }
    return path;
  }

  startMovement() {
    if (!this.pathCoords || this.pathCoords.length === 0) {
      console.error("Path coordinates are not defined.");
      return;
    }

    const moveStep = () => {
      const nextCoordIndex =
        (this.currentCoordIndex + 1) % this.pathCoords.length;
      const currentCoord = this.pathCoords[this.currentCoordIndex];
      const nextCoord = this.pathCoords[nextCoordIndex];

      const distance = this.calculateDistance(
        currentCoord.lat,
        currentCoord.lng,
        nextCoord.lat,
        nextCoord.lng
      );
      const speed = this.speed;
      const travelTime = (distance / speed) * 3600 * 1000;
      const steps = Math.floor(travelTime / 500);
      const latStep = (nextCoord.lat - currentCoord.lat) / steps;
      const lngStep = (nextCoord.lng - currentCoord.lng) / steps;

      let currentStep = 0;

      const animate = () => {
        if (currentStep < steps) {
          currentCoord.lat += latStep;
          currentCoord.lng += lngStep;
          this.position = L.latLng(currentCoord.lat, currentCoord.lng);
          this.marker.setLatLng(this.position);

          if (currentStep > 0 || this.currentCoordIndex !== 0) {
            this.trajectory.push(this.position);

            if (this.trajectory.length > 60) {
              this.trajectory.shift();
            }

            if (this.polylineVisible) {
              this.polyline.setLatLngs(this.trajectory);
            }
          }

          currentStep++;
          setTimeout(animate, 500);
        } else {
          this.currentCoordIndex = nextCoordIndex;
          moveStep();
        }
      };

      animate();
    };

    moveStep();
  }

  showDetails() {
    MovingObject.closeModal();

    const modal = this.modal;
    const objectType = document.getElementById("objectType");
    const objectSpeed = document.getElementById("objectSpeed");
    const objectPosition = document.getElementById("objectPosition");

    objectType.textContent = `Type: ${this.type}`;
    objectSpeed.textContent = `Speed: ${this.speed.toFixed(2)} km/h`;
    objectPosition.textContent = `Position: LatLng(${this.position.lat.toFixed(
      4
    )}, ${this.position.lng.toFixed(4)})`;

    modal.style.display = "flex";
    modal.style.justifyContent = "center";
    modal.style.alignItems = "center";

    if (this.fullPathPolyline) {
      this.map.removeLayer(this.fullPathPolyline);
    }

    this.fullPathPolyline = L.polyline(this.getFullPath(), {
      color: this.getColor(),
      weight: 3,
      opacity: 0.7,
      dashArray: "5, 10",
    }).addTo(this.map);

    if (!this.polyline._map) {
      this.map.addLayer(this.polyline);
      this.polyline.setLatLngs(this.trajectory);
      this.polylineVisible = true;
    }

    this.reinitializeGeodesicLines();

    MovingObject.currentObject = this;

    const closeModal = () => {
      modal.style.display = "none";
      modal.removeEventListener("click", closeModal);
      cancelAnimationFrame(this.modalAnimationFrame);
      if (this.fullPathPolyline) {
        this.map.removeLayer(this.fullPathPolyline);
        this.fullPathPolyline = null;
      }
      if (this.polyline._map) {
        this.map.removeLayer(this.polyline);
        this.polylineVisible = false;
      }
      if (this.geodesicLines.length > 0) {
        this.geodesicLines.forEach((line) => {
          if (line._map) {
            this.map.removeLayer(line);
          }
        });
        this.geodesicLines = [];
      }
      this.stopUpdatingModal();
      MovingObject.currentObject = null;
    };

    modal.querySelector(".close").addEventListener("click", closeModal);
    modal.addEventListener("click", function (event) {
      if (event.target === modal) {
        closeModal();
      }
    });

    this.updateModalPosition();
    this.startUpdatingModal();
  }

  calculateDistance(lat1, lon1, lat2, lon2) {
    const R = 6371;
    const dLat = this.degToRad(lat2 - lat1);
    const dLon = this.degToRad(lon2 - lon1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.degToRad(lat1)) *
        Math.cos(this.degToRad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c;
  }

  degToRad(deg) {
    return deg * (Math.PI / 180);
  }

  getSpeed() {
    return getRandomSpeed(110, 300);
  }
}

export default Circle;
