import { getRandomSpeed } from "../utils/getRandomSpeed";
import MovingObject from "./MovingObject";

class Triangle extends MovingObject {
  constructor(map, startPosition) {
    super("triangle", map, startPosition);
    this.map = map;
    this.destination = this.randomDestination();
    this.distance = this.calculateDistance();
    this.travelledDistance = 0;
    this.maxTime = 60 * 60 * 1000;
    this.timeout = setTimeout(() => this.remove(), this.maxTime);
    this.speed = this.getSpeed();
    this.fullPathPolyline = null;

    this.addClickEventListener();
  }

  randomDestination() {
    const maxLat = 90;
    const maxLng = 180;
    return L.latLng(
      (Math.random() - 0.5) * maxLat,
      (Math.random() - 0.5) * maxLng
    );
  }

  calculateDistance() {
    if (!this.destination) return 0;
    const R = 6371;
    const lat1 = this.position.lat * (Math.PI / 180);
    const lon1 = this.position.lng * (Math.PI / 180);
    const lat2 = this.destination.lat * (Math.PI / 180);
    const lon2 = this.destination.lng * (Math.PI / 180);
    const dlat = lat2 - lat1;
    const dlon = lon2 - lon1;
    const a =
      Math.sin(dlat / 2) ** 2 +
      Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlon / 2) ** 2;
    return R * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  }

  generateNewPosition() {
    if (!this.destination) return this.position;
    const R = 6371;
    const stepDistance = this.speed / 3600 / R / 10;
    const bearing = this.bearingToDestination();
    const lat1 = this.position.lat * (Math.PI / 180);
    const lon1 = this.position.lng * (Math.PI / 180);
    const lat2 = Math.asin(
      Math.sin(lat1) * Math.cos(stepDistance) +
        Math.cos(lat1) * Math.sin(stepDistance) * Math.cos(bearing)
    );
    const lon2 =
      lon1 +
      Math.atan2(
        Math.sin(bearing) * Math.sin(stepDistance) * Math.cos(lat1),
        Math.cos(stepDistance) - Math.sin(lat1) * Math.sin(lat2)
      );
    const newLat = lat2 * (180 / Math.PI);
    const newLng = lon2 * (180 / Math.PI);

    if (isNaN(newLat) || isNaN(newLng)) {
      console.error("Invalid position calculated:", { newLat, newLng });
      return this.position;
    }

    this.travelledDistance += this.calculateDistanceBetweenPoints(
      this.position,
      L.latLng(newLat, newLng)
    );
    if (this.travelledDistance >= this.distance) {
      this.travelledDistance = this.distance;
      return this.destination;
    }

    return L.latLng(newLat, newLng);
  }

  bearingToDestination() {
    const lat1 = this.position.lat * (Math.PI / 180);
    const lon1 = this.position.lng * (Math.PI / 180);
    const lat2 = this.destination.lat * (Math.PI / 180);
    const lon2 = this.destination.lng * (Math.PI / 180);
    const dlon = lon2 - lon1;
    const y = Math.sin(dlon) * Math.cos(lat2);
    const x =
      Math.cos(lat1) * Math.sin(lat2) -
      Math.sin(lat1) * Math.cos(lat2) * Math.cos(dlon);
    return Math.atan2(y, x);
  }

  calculateDistanceBetweenPoints(p1, p2) {
    const R = 6371;
    const lat1 = p1.lat * (Math.PI / 180);
    const lon1 = p1.lng * (Math.PI / 180);
    const lat2 = p2.lat * (Math.PI / 180);
    const lon2 = p2.lng * (Math.PI / 180);
    const dlat = lat2 - lat1;
    const dlon = lon2 - lon1;
    const a =
      Math.sin(dlat / 2) ** 2 +
      Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlon / 2) ** 2;
    return R * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  }

  getSpeed() {
    return getRandomSpeed(1700, 2200);
  }

  getFullPath() {
    if (!this.destination) return [this.startPosition];

    const path = [];
    let currentPosition = this.startPosition;
    let currentTravelledDistance = 0;

    path.push(currentPosition);

    const steps = 1000;
    const fractionIncrement = 1 / steps;

    for (let i = 1; i <= steps; i++) {
      const fraction = fractionIncrement * i;
      const newPosition = this.interpolate(
        currentPosition,
        this.destination,
        fraction
      );

      if (!newPosition || isNaN(newPosition.lat) || isNaN(newPosition.lng)) {
        console.error("Invalid position interpolated:", newPosition);
        break;
      }

      const segmentDistance = this.calculateDistanceBetweenPoints(
        currentPosition,
        newPosition
      );

      currentTravelledDistance += segmentDistance;

      path.push(newPosition);
      currentPosition = newPosition;

      if (currentTravelledDistance >= this.distance) {
        path.push(this.destination);
        break;
      }
    }

    return path;
  }

  interpolate(start, end, fraction) {
    const lat1 = start.lat * (Math.PI / 180);
    const lon1 = start.lng * (Math.PI / 180);
    const lat2 = end.lat * (Math.PI / 180);
    const lon2 = end.lng * (Math.PI / 180);

    const d =
      2 *
      Math.asin(
        Math.sqrt(
          Math.sin((lat2 - lat1) / 2) ** 2 +
            Math.cos(lat1) * Math.cos(lat2) * Math.sin((lon2 - lon1) / 2) ** 2
        )
      );

    const A = Math.sin((1 - fraction) * d) / Math.sin(d);
    const B = Math.sin(fraction * d) / Math.sin(d);

    const x =
      A * Math.cos(lat1) * Math.cos(lon1) + B * Math.cos(lat2) * Math.cos(lon2);
    const y =
      A * Math.cos(lat1) * Math.sin(lon1) + B * Math.cos(lat2) * Math.sin(lon2);
    const z = A * Math.sin(lat1) + B * Math.sin(lat2);

    const lat = Math.atan2(z, Math.sqrt(x * x + y * y));
    const lon = Math.atan2(y, x);

    const newLat = lat * (180 / Math.PI);
    const newLng = lon * (180 / Math.PI);

    if (isNaN(newLat) || isNaN(newLng)) {
      console.error("Invalid interpolation:", { newLat, newLng });
      return null;
    }

    return L.latLng(newLat, newLng);
  }

  addClickEventListener() {
    this.marker.on("click", () => {
      const fullPath = this.getFullPath();
      if (!fullPath.length || fullPath.some((point) => point === null)) {
        console.error("Invalid full path:", fullPath);
        return;
      }

      if (this.fullPathPolyline) {
        this.map.removeLayer(this.fullPathPolyline);
      }
      this.fullPathPolyline = L.polyline(fullPath, {
        color: "yellow",
        dashArray: "5, 10",
      }).addTo(this.map);
    });
  }

  remove() {
    console.log("Removing triangle and related elements");

    if (this.marker) {
      this.map.removeLayer(this.marker);
      console.log("Marker removed");
      this.marker = null;
    } else {
      console.log("Marker was already null or not found");
    }

    if (this.fullPathPolyline) {
      this.map.removeLayer(this.fullPathPolyline);
      console.log("Polyline removed");
      this.fullPathPolyline = null;
    } else {
      console.log("Polyline was already null or not found");
    }

    clearTimeout(this.timeout);
    console.log("Timeout cleared");

    if (MovingObject.currentObject === this) {
      MovingObject.closeModal();
    }

    const modal = document.getElementById("modal");
    if (modal) {
      modal.style.display = "none";
      console.log("Modal hidden");
    } else {
      console.error("Modal not found");
    }
  }
}

export default Triangle;
