import { getRandomSpeed } from "../utils/getRandomSpeed";

class MovingObject {
  static currentObject = null;
  static objectCount = 0;
  static maxObjects = 5000;

  constructor(type, map, startPosition) {
    if (MovingObject.objectCount >= MovingObject.maxObjects) {
      throw new Error(
        `Cannot create more than ${MovingObject.maxObjects} objects.`
      );
    }

    MovingObject.objectCount++;

    this.type = type;
    this.map = map;
    this.position = startPosition;
    this.startPosition = startPosition;
    this.speed = this.getSpeed();
    this.marker = this.createMarker();
    this.map.addLayer(this.marker);
    this.trajectory = [startPosition];
    this.polyline = L.polyline([startPosition], { color: this.getColor() });
    this.polylineVisible = false;
    this.startTime = Date.now();
    this.geodesicLines = [];
    this.fullPathPolyline = null;

    this.marker.on("click", () => this.showDetails());
    this.updateStartMarker();
    this.startMovement();
    this.modal = document.getElementById("modal");
    this.updateModalPosition();
  }

  static closeModal() {
    const modal = document.getElementById("modal");
    if (MovingObject.currentObject) {
      const currentObject = MovingObject.currentObject;

      if (currentObject.polylineVisible) {
        currentObject.map.removeLayer(currentObject.polyline);
        currentObject.polylineVisible = false;
      }

      if (currentObject.geodesicLines.length > 0) {
        currentObject.geodesicLines.forEach((line) => {
          if (line._map) {
            currentObject.map.removeLayer(line);
          }
        });
        currentObject.geodesicLines = [];
      }

      if (currentObject.fullPathPolyline) {
        currentObject.map.removeLayer(currentObject.fullPathPolyline);
        currentObject.fullPathPolyline = null;
      }

      currentObject.stopUpdatingModal();
      MovingObject.currentObject = null;
    }

    modal.style.display = "none";

    modal
      .querySelector(".close")
      .removeEventListener("click", MovingObject.closeModal);
    modal.removeEventListener("click", MovingObject.handleOutsideClick);

    cancelAnimationFrame(MovingObject.modalAnimationFrame);
  }

  clearOverlays() {
    if (this.polylineVisible) {
      this.map.removeLayer(this.polyline);
      this.polylineVisible = false;
    }

    if (this.geodesicLines.length > 0) {
      this.geodesicLines.forEach((line) => {
        if (line._map) {
          this.map.removeLayer(line);
        }
      });
      this.geodesicLines = [];
    }

    if (this.fullPathPolyline) {
      this.map.removeLayer(this.fullPathPolyline);
      this.fullPathPolyline = null;
    }
  }

  static handleOutsideClick(event) {
    const modal = document.getElementById("modal");
    if (event.target === modal) {
      MovingObject.closeModal();
    }
  }

  createMarker() {
    const icon = L.divIcon({ className: `moving-object ${this.type}` });
    return L.marker(this.position, { icon: icon });
  }

  updateStartMarker() {
    const svgIcon = L.divIcon({
      className: "start-marker",
      html: `
        <svg
          xmlns="http://www.w3.org/2000/svg"
          data-name="Layer 1"
          viewBox="0 0 95 118.75"
          x="0px"
          y="0px"
        >
          <path
            d="M47.5,1A34.459,34.459,0,0,0,13.041,35.459C13.041,54.49,31.736,75.08,47.5,94,63.264,75.08,81.959,54.49,81.959,35.459A34.459,34.459,0,0,0,47.5,1Zm0,50.879a16.42,16.42,0,1,1,16.42-16.42A16.451,16.451,0,0,1,47.5,51.879Z"
            fill-rule="evenodd"
          />
        </svg>
      `,
      iconSize: [24, 24],
      iconAnchor: [12, 20],
    });
    this.startMarker = L.marker(this.startPosition, { icon: svgIcon });
    this.map.addLayer(this.startMarker);
  }

  getColor() {
    const colors = {
      square: "blue",
      circle: "green",
      triangle: "yellow",
    };
    return colors[this.type] || "red";
  }

  startMovement() {
    this.move();
    this.interval = setInterval(() => this.move(), 1000);
  }

  move() {
    const newPosition = this.generateNewPosition();
    if (newPosition) {
      this.position = newPosition;
      this.marker.setLatLng(newPosition);
      this.trajectory.push(newPosition);

      if (this.polylineVisible) {
        this.polyline.addLatLng(newPosition);
      }

      if (this.trajectory.length > 60) {
        this.trajectory.shift();
        if (this.polylineVisible) {
          this.polyline.setLatLngs(this.trajectory);
        }
      }

      if (MovingObject.currentObject === this) {
        this.updateModalData();
      }
    }
  }

  generateNewPosition() {
    return this.position;
  }

  showDetails() {
    MovingObject.closeModal();

    const modal = this.modal;
    const objectType = document.getElementById("objectType");
    const objectSpeed = document.getElementById("objectSpeed");
    const objectPosition = document.getElementById("objectPosition");
    const timeToExpire = document.getElementById("timeToExpire");

    objectType.textContent = `Type: ${this.type}`;
    objectSpeed.textContent = `Speed: ${this.speed.toFixed(2)} km/h`;
    objectPosition.textContent = `Position: LatLng(${this.position.lat.toFixed(
      4
    )}, ${this.position.lng.toFixed(4)})`;
    timeToExpire.textContent = `Time to expire: ${this.timeToExpire()}`;

    modal.style.display = "flex";
    modal.style.justifyContent = "center";
    modal.style.alignItems = "center";

    if (!this.polylineVisible) {
      this.polyline.setLatLngs(this.trajectory);
      this.map.addLayer(this.polyline);
      this.polylineVisible = true;
    }

    this.createGeodesicPath();
    this.reinitializeGeodesicLines();

    MovingObject.currentObject = this;

    const closeModal = () => {
      this.clearOverlays();
      modal.style.display = "none";
      modal.querySelector(".close").removeEventListener("click", closeModal);
      modal.removeEventListener("click", outsideClickListener);
      cancelAnimationFrame(this.modalAnimationFrame);
      this.stopUpdatingModal();
      MovingObject.currentObject = null;
    };

    const outsideClickListener = (event) => {
      if (event.target === modal) {
        closeModal();
      }
    };

    modal.querySelector(".close").addEventListener("click", closeModal);
    modal.addEventListener("click", outsideClickListener);

    this.updateModalPosition();
    this.startUpdatingModal();
  }

  createGeodesicPath() {}

  reinitializeGeodesicLines() {
    if (this.geodesicLines.length > 0) {
      this.geodesicLines.forEach((line) => {
        if (!line._map) {
          this.map.addLayer(line);
        }
      });
    }
  }

  startUpdatingModal() {
    if (!this.modalUpdateInterval) {
      this.modalUpdateInterval = setInterval(() => {
        this.updateModalData();
      }, 1000);
    }
  }

  stopUpdatingModal() {
    if (this.modalUpdateInterval) {
      clearInterval(this.modalUpdateInterval);
      this.modalUpdateInterval = null;
    }
  }

  updateModalData() {
    const objectType = document.getElementById("objectType");
    const objectSpeed = document.getElementById("objectSpeed");
    const objectPosition = document.getElementById("objectPosition");
    const timeToExpire = document.getElementById("timeToExpire");

    objectType.textContent = `Type: ${this.type}`;
    objectSpeed.textContent = `Speed: ${this.speed.toFixed(2)} km/h`;
    objectPosition.textContent = `Position: LatLng(${this.position.lat.toFixed(
      4
    )}, ${this.position.lng.toFixed(4)})`;
    timeToExpire.textContent =
      this.type !== "triangle" ? "" : `Time to expire: ${this.timeToExpire()}`;
  }

  renderFullPath() {
    this.polyline.setLatLngs(this.pathCoords);
  }

  getFullPath() {
    return [];
  }

  updateModalPosition() {
    const point = this.map.latLngToContainerPoint(this.position);
    this.modal.style.left = `${point.x}px`;
    this.modal.style.top = `${point.y}px`;
    this.modalAnimationFrame = requestAnimationFrame(() =>
      this.updateModalPosition()
    );
  }

  timeToExpire() {
    const msRemaining = Math.max(0, 3600000 - (Date.now() - this.startTime));
    const minutes = Math.floor((msRemaining % 3600000) / 60000);
    const seconds = Math.floor((msRemaining % 60000) / 1000);
    return `${minutes} minutes ${seconds} seconds`;
  }

  stopMovement() {
    clearInterval(this.interval);
  }

  remove() {
    this.stopMovement();
    this.map.removeLayer(this.marker);
    this.clearOverlays();
    MovingObject.objectCount--;
  }

  getSpeed() {
    return getRandomSpeed();
  }
}

export default MovingObject;
